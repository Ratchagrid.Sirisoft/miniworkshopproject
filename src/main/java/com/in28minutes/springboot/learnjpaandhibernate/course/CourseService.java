package com.in28minutes.springboot.learnjpaandhibernate.course;

import com.in28minutes.springboot.learnjpaandhibernate.course.springdatajpa.CourseSpringDataJpaRepository;
import com.in28minutes.springboot.learnjpaandhibernate.response.Response;
import com.in28minutes.springboot.learnjpaandhibernate.student.Student;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class CourseService {

    @Autowired
    CourseSpringDataJpaRepository repository;

    public ResponseEntity<Response> insertCourse(Course course) {
        Course responseCourse = repository.save(course);
        Response<Course> response = new Response<>(responseCourse, "200", "OK");
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    public ResponseEntity<Response> getCourseFilter() {
        List<Course> courseList = repository.findByDeletedAtIsNull();
        Response<List<Course>> successResponse = new Response<>(courseList, "200", "OK");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> foreRemoveCourse(String courseId) {
        Course course = repository.findById(Long.parseLong(courseId)).orElse(null);
        if(course == null) {
            Response<String> failResponse = new Response<>("Course not found", "404", "NOT_FOUND");
            return new ResponseEntity<>(failResponse, HttpStatus.NOT_FOUND);
        }
        repository.delete(course);

        Response<String> successResponse = new Response<>("Course delete success", "200", "OK");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> softDeleteCourse(String courseId) {
        Course course = repository.findById(Long.parseLong(courseId)).orElse(null);
        if(course == null) {
            Response<String> failResponse = new Response<>("Course not found", "404", "NOT_FOUND");
            return new ResponseEntity<>(failResponse, HttpStatus.NOT_FOUND);
        }

        course.setDeletedAt(LocalDateTime.now());
        Course updatedCourse = repository.save(course);

        if(updatedCourse == null) {
            Response<String> failResponse = new Response<>("Database Err", "500", "INTERNAL_SERVER_ERROR");
            return new ResponseEntity<>(failResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Response<Course> successResponse = new Response<>(updatedCourse, "200", "OK");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> getCourseById(String courseId) {
        System.out.println(System.getenv("MYSQL_HOST"));
        Course course = repository.findById(Long.parseLong(courseId)).orElse(null);
        if(course == null) {
            Response<String> failedResponse = new Response<>("Course not found", "404", "NOT_FOUND");
            return new ResponseEntity<>(failedResponse, HttpStatus.NOT_FOUND);
        }

        Response<Course> successResponse = new Response<>(course, "200", "OK");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> updateCourseById(String courseId, Map<String, String> formData) {
        Course currentCourse = repository.findById(Long.parseLong(courseId)).orElse(null);
        if(currentCourse == null) {
            Response<String> failedResponse = new Response<>("Course not found", "404", "NOT_FOUND");
            return new ResponseEntity<>(failedResponse, HttpStatus.NOT_FOUND);
        }
        currentCourse.setName((String)formData.get("name"));
        currentCourse.setAuthor(((String)formData.get("author")));
        System.out.println(currentCourse.getName());
        System.out.println(currentCourse.getAuthor());
        repository.save(currentCourse);
        Response<String> successResponse = new Response<>("Course updated success", "200", "OK");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

}
