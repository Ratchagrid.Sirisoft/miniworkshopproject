package com.in28minutes.springboot.learnjpaandhibernate.course.jpa;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import com.in28minutes.springboot.learnjpaandhibernate.course.jdbc.CourseJdbcRepository;
import com.in28minutes.springboot.learnjpaandhibernate.course.springdatajpa.CourseSpringDataJpaRepository;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CourseJpaCommandLineRunner implements CommandLineRunner {

//    @Autowired
//    private CourseJpaRepository courseJpaRepository;

    @Autowired
    private CourseSpringDataJpaRepository repository;

    @Override
    public void run(String... args) throws Exception {
//        Course course1 = new Course(1, "Learn AWS Now", "in28minutes");
//        Course course2 = new Course(2, "Learn Docker", "in28minutes");
//        Course course3 = new Course(3, "Learn Basic Backend Developer", "in28minutes");
//        Course course4 = new Course(4, "Learn Java Spring Boot", "in28minutes");
//        repository.save(course1);
//        repository.save(course2);
//        repository.save(course3);
//        repository.save(course4);
    }
}
