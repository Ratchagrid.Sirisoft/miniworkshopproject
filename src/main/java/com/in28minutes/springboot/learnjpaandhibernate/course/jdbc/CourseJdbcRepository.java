package com.in28minutes.springboot.learnjpaandhibernate.course.jdbc;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


@Repository
public class CourseJdbcRepository {

    @Autowired
    private JdbcTemplate springJdbcTemplate;

    private static String INSERT_QUERY = """
            insert into course(id,name,author)
            values(?,?,?)
            """;

    private static String DELETE_QUERY_ID = """
            delete from course
            where course.id = ?
            """;

    private static String SEARCH_ID = """
            select * from course where course.id = ?
            """;

    public void insert(Course course) {
        springJdbcTemplate.update(INSERT_QUERY, course.getId(), course.getName(), course.getAuthor());
    }

    public void delete(long id) {
        springJdbcTemplate.update(DELETE_QUERY_ID, id);
    }

    public Course getCourseById(String id) {
        return springJdbcTemplate.queryForObject(SEARCH_ID, new BeanPropertyRowMapper<>(Course.class), id);
    }


}
