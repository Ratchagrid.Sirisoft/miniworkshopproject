//package com.in28minutes.springboot.learnjpaandhibernate.course.jdbc;
//
//import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
//import com.in28minutes.springboot.learnjpaandhibernate.course.jpa.CourseJpaRepository;
//import jakarta.persistence.PersistenceContext;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.CommandLineRunner;
//import org.springframework.stereotype.Component;
//
//@Component
//public class CourseJdbcCommandLineRunner implements CommandLineRunner {
//
//    @Autowired
//    private CourseJdbcRepository courseJdbcRepository;
//
//    @PersistenceContext
//    private CourseJpaRepository courseJpaRepository;
//
//    @Override
//    public void run(String... args) throws Exception {
////        Course course1 = new Course(1, "Learn AWS Now", "in28minutes");
////        Course course2 = new Course(2, "Learn Docker", "in28minutes");
////        Course course3 = new Course(3, "Learn Basic Backend Developer", "in28minutes");
////        Course course4 = new Course(4, "Learn Java Spring Boot", "in28minutes");
////        courseJpaRepository.insert(course1);
////        courseJpaRepository.insert(course2);
////        courseJpaRepository.insert(course3);
////        courseJpaRepository.insert(course4);
//    }
//}
