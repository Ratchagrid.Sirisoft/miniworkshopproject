package com.in28minutes.springboot.learnjpaandhibernate.course.springdatajpa;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CourseSpringDataJpaRepository extends JpaRepository<Course, Long> {

    List<Course> findByAuthor(String author);
    List<Course> findByName(String name);

    List<Course> findByNameLike(String name);

    List<Course> findByAuthorLike(String author);

//    @Query(value = "SELECT * FROM course WHERE ", nativeQuery = true)
//    List<Course> findCourseWhereDeletdAtNull();

    List<Course> findByDeletedAtIsNull();
}
