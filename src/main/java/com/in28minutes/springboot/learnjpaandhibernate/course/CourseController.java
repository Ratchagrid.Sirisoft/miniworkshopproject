package com.in28minutes.springboot.learnjpaandhibernate.course;

import com.in28minutes.springboot.learnjpaandhibernate.course.springdatajpa.CourseSpringDataJpaRepository;
import com.in28minutes.springboot.learnjpaandhibernate.response.Response;
import com.in28minutes.springboot.learnjpaandhibernate.student.Student;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.ErrorResponseException;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class CourseController {

    @Autowired
    private CourseSpringDataJpaRepository repository;
    @Autowired
    private CourseService courseService;

    @RequestMapping(value = "/course/{courseId}", method = RequestMethod.GET)
    public ResponseEntity<Response> getCourseById(@PathVariable String courseId) {
        return courseService.getCourseById(courseId);
    }

    @RequestMapping(value = "/course", method = RequestMethod.POST)
    public ResponseEntity<Response> insertCourse(@Valid @RequestBody Course course, BindingResult result) {
        if(result.hasErrors()) {
            List<String> errorMessages = result.getFieldErrors().stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.toList());
            Response<List> response = new Response<>(errorMessages, "400", "Bad_Request");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return courseService.insertCourse(course);
    }

    @RequestMapping(value = "/course/{courseId}", method = RequestMethod.DELETE)
    public ResponseEntity deleteCourse(@PathVariable String courseId) {
        return courseService.foreRemoveCourse(courseId);
    }

    @GetMapping("/course/get")
    public ResponseEntity<Response> getCourseList() {
        return courseService.getCourseFilter();
    }

    @DeleteMapping("/course/delete/soft/{courseId}")
    public ResponseEntity<Response> softDeleteCourse(@PathVariable String courseId) {
        return courseService.softDeleteCourse(courseId);
    }

    @PutMapping("/course/{courseId}")
    public ResponseEntity<Response> updateCourseById(@PathVariable String courseId, @RequestParam Map<String, String> formData) {
        return courseService.updateCourseById(courseId, formData);
    }

}
