package com.in28minutes.springboot.learnjpaandhibernate.student.jpa;

import com.in28minutes.springboot.learnjpaandhibernate.student.Student;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class StudentJpaRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void insert(Student student) {
        entityManager.merge(student);
    }

    public Student findById(String id) {
        return entityManager.find(Student.class, id);
    }

    public void update(Student student) {
        entityManager.merge(student);
    }

    public void deleteById(String id) {
        Student student = entityManager.find(Student.class, id);
        entityManager.remove(student);
    }

}
