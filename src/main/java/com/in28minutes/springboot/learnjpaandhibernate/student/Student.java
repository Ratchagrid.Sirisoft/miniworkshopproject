package com.in28minutes.springboot.learnjpaandhibernate.student;

import com.in28minutes.springboot.learnjpaandhibernate.Enrollment.Enrollment;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "student")
public class Student {
    public Student(long id, String name, String age) {
        super();
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    @Id
    @GeneratedValue
    private long id;

    @NotBlank(message="Name is required")
    private String name;

    @NotBlank(message="Age is required")
    private String age;

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    @OneToMany(mappedBy = "student", cascade = CascadeType.REMOVE)
    private List<Enrollment> enrollments;

    public void setDeletedAt(LocalDateTime deletedAt) {
        this.deletedAt = deletedAt;
    }

    @Override
    public String toString() {
        return "Student{" +
                ", name='" + name + '\'' +
                ", age='" + age + '\'' +
                ", createdAt=" + createdAt +
                ", updatedAt=" + updatedAt +
                ", deletedAt=" + deletedAt +
                '}';
    }

    public LocalDateTime getDeletedAt() {
        return deletedAt;
    }

    @Column(name = "created_at", nullable = false, updatable = false,
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    @CreationTimestamp
    private LocalDateTime createdAt;

    @Column(name = "updated_at", nullable = false)
    @UpdateTimestamp
    private LocalDateTime updatedAt;

    @Column(name = "deleted_at")
    private LocalDateTime deletedAt;

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

}
