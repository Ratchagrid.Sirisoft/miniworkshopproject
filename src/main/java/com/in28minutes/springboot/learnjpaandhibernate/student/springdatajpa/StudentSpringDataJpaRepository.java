package com.in28minutes.springboot.learnjpaandhibernate.student.springdatajpa;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import com.in28minutes.springboot.learnjpaandhibernate.student.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentSpringDataJpaRepository extends JpaRepository<Student, Long> {

    List<Student> findByName(String name);

    List<Student> findByAge(String age);

    List<Student> findByNameLike(String name);

    List<Student> findByAgeLike(String age);

    List<Student> findByDeletedAtIsNull();

    Student findStudentByIdAndDeletedAtIsNull(Long studentId);
}
