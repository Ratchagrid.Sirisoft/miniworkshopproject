package com.in28minutes.springboot.learnjpaandhibernate.student;


import com.in28minutes.springboot.learnjpaandhibernate.response.Response;
import com.in28minutes.springboot.learnjpaandhibernate.student.springdatajpa.StudentSpringDataJpaRepository;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class StudentController {
    @Autowired
    private StudentSpringDataJpaRepository repository;

    @Autowired
    private StudentService studentService;
    
    @RequestMapping(value = "/student", method = RequestMethod.POST)
    public ResponseEntity<Response> insertStudent(@Valid @RequestBody Student student, BindingResult result) {

        if(result.hasErrors()) {
            List<String> errorMessages = result.getFieldErrors().stream()
                    .map(FieldError::getDefaultMessage)
                    .collect(Collectors.toList());
            Response<List> response = new Response<>(errorMessages, "400", "Bad_Request");
            return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
        }
        return studentService.insertStudent(student);
    }

    @RequestMapping(value = "/student/{studentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Response> removeStudent(@PathVariable String studentId) {
        return studentService.removeStudent(studentId);
    }

    @RequestMapping(value = "/student/delete/{studentId}", method = RequestMethod.DELETE)
    public ResponseEntity<Response> actuallyRemoveStudent(@PathVariable String studentId) {
        return studentService.actualRemoveStudent(studentId);
    }

    @GetMapping(value="/students")
    public ResponseEntity<Response> getStudentList() {
        return studentService.getStudentListWithFilter();
    }

    @GetMapping(value = "/student/{studentId}")
    public ResponseEntity<Response> getStudentById(@PathVariable String studentId) {
        return studentService.getStudentById(studentId);
    }



}
