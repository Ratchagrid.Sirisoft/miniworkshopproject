package com.in28minutes.springboot.learnjpaandhibernate.student;

import com.in28minutes.springboot.learnjpaandhibernate.response.Response;
import com.in28minutes.springboot.learnjpaandhibernate.response.ResponseMapper;
import com.in28minutes.springboot.learnjpaandhibernate.student.springdatajpa.StudentSpringDataJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentSpringDataJpaRepository repository;

    @Autowired
    private ResponseMapper responseMapper;

    public ResponseEntity<Response> insertStudent(Student student) {
        Student savedStudent = repository.save(student);
        Response<Student> doneResponse = responseMapper.success(savedStudent);
        return new ResponseEntity<>(doneResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> removeStudent(String studentId) {
        Student student = repository.findById(Long.parseLong(studentId)).orElse(null);
        if(student == null) {
            Response<String> failResponse = responseMapper.dataNotFound("Student");
            return new ResponseEntity<>(failResponse, HttpStatus.NOT_FOUND);
        }

        student.setDeletedAt(LocalDateTime.now());
        Student updatedStudent = repository.save(student);

        if(updatedStudent == null) {
            Response<String> failResponse = new Response<>("Database Err", "500", "INTERNAL_SERVER_ERROR");
            return new ResponseEntity<>(failResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        Response<Student> doneResponse = responseMapper.success(updatedStudent);
        return new ResponseEntity<>(doneResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> actualRemoveStudent(String studentId) {
        Student student = repository.findById(Long.parseLong(studentId)).orElse(null);
        if(student == null) {
            Response<String> failResponse = responseMapper.dataNotFound("Student");
            return new ResponseEntity<>(failResponse, HttpStatus.NOT_FOUND);
        }
        repository.delete(student);
        Response<String> successResponse = responseMapper.success("Done remove");
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> getStudentListWithFilter() {
        List<Student> studentList = repository.findByDeletedAtIsNull();
        Response<List<Student>> successResponse = responseMapper.success(studentList);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }

    public ResponseEntity<Response> getStudentById(String studentId) {
        Student student = repository.findStudentByIdAndDeletedAtIsNull(Long.parseLong(studentId));
        Response<Student> successResponse = responseMapper.success(student);
        return new ResponseEntity<>(successResponse, HttpStatus.OK);
    }


}
