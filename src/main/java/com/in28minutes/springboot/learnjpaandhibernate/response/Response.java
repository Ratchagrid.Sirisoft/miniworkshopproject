package com.in28minutes.springboot.learnjpaandhibernate.response;

public class Response<T> {

    private String status_code;
    private String status_message;
    private T msg;

    public Response(T msg, String status_code, String status_message) {
        super();
        this.status_code = status_code;
        this.status_message = status_message;
        this.msg = msg;
    }

    public String getStatus_code() {
        return status_code;
    }

    public String getStatus_message() {
        return status_message;
    }

    public T getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "Response{" +
                "status_code='" + status_code + '\'' +
                ", status_message='" + status_message + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
