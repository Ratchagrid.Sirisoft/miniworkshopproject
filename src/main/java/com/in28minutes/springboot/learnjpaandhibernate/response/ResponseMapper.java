package com.in28minutes.springboot.learnjpaandhibernate.response;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ResponseMapper {

    public Response dataNotFound(String columnName) {
        String temp = columnName + "Not Found";
        Response<String> response = new Response<>(temp, "404", "NOT_FOUND");
        return response;
    }

    public <T> Response<T> success(T value) {
        Response<T> response = new Response<>(value, "200", "OK");
        return response;
    }



}
