package com.in28minutes.springboot.learnjpaandhibernate.Enrollment;


import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

@Repository
@Transactional
public class EnrollmentJpaRepository {

    @PersistenceContext
    private EntityManager entityManager;

    public void insert(Enrollment enrollment) {
        entityManager.merge(enrollment);
    }

    public void findById(String id) {
        entityManager.find(Enrollment.class, id);
    }

    public void deleteById(String id) {
        Enrollment enrollment = entityManager.find(Enrollment.class, id);
        entityManager.remove(enrollment);
    }

}
