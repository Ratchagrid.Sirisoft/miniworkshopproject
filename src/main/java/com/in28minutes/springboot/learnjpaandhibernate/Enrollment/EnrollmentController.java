package com.in28minutes.springboot.learnjpaandhibernate.Enrollment;

import com.in28minutes.springboot.learnjpaandhibernate.course.Course;
import com.in28minutes.springboot.learnjpaandhibernate.course.springdatajpa.CourseSpringDataJpaRepository;
import com.in28minutes.springboot.learnjpaandhibernate.response.Response;
import com.in28minutes.springboot.learnjpaandhibernate.student.Student;
import com.in28minutes.springboot.learnjpaandhibernate.student.springdatajpa.StudentSpringDataJpaRepository;
import jakarta.transaction.Transactional;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@RestController
public class EnrollmentController {

    @Autowired
    private EnrollmentSpringDataJpaRepository repository;

    @Autowired
    private StudentSpringDataJpaRepository studentRepository;
    @Autowired
    private CourseSpringDataJpaRepository courseRepository;


    @RequestMapping("/enrollment/create/{studentId}/{courseId}")
    public ResponseEntity<Response> generateEnrollment(@PathVariable String studentId, @PathVariable String courseId) {


        Student student = studentRepository.findById(Long.parseLong(studentId)).get();
        Course course = courseRepository.findById(Long.parseLong(courseId)).get();

        Enrollment preEnrollment = new Enrollment(student, course);
        Enrollment enrollment = repository.save(preEnrollment);
        Response<Long> doneResponse = new Response<>(enrollment.getId(), "200", "OK");
        return new ResponseEntity<>(doneResponse, HttpStatus.OK);

    }


}
