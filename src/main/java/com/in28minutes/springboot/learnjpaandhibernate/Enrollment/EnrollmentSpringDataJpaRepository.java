package com.in28minutes.springboot.learnjpaandhibernate.Enrollment;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EnrollmentSpringDataJpaRepository extends JpaRepository<Enrollment, Long> {

}
